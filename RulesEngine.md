#Rules Engine

##Glossary
#####RulesEngine
>is the name of application including all bundles.

#####RulesEngineBundle
>is the bundle that will be entry point to all other bundles. We should not give direct access to any other bundles. This bundle will handle calls using (Controller) only and then use (forward) method to call other controllers from other bundles.

#####Module
> is the business definition for bundle that will deal with a particular part of RulesEngine. We will have the following:
> 
> * **Logistics Module**
> * **Day Of Choice Module**
> * **Stock Module**
> * **Product Module**

---

##Structure
- **Bundle Name**
	* **Constants**
		- Any constants related to bundle must be here
	* **Controller**
		- All calls to this bundle that will be made from main entry should be here. So we can use the component separately and independat of other modules.
		- There should NOT be any routing to these controllers, all calls must be made through main bundle `RulesEngineBundle` which will use `forward` to call these controllers/actions. Checkout [Calling a controller from another controller in Symfony](http://symfony.com/doc/current/book/controller.html#forwarding-to-another-controller) for more info.
	* **DependencyInjuction**
	* **DTO**
		- All inputs to each bundle wheather its single entry or bulk should be handled by DTO.
	* **Entity**
		- Each bundle (apart from `RulesEngineBundle`) should have ***at least*** one entity which is `RulesConfig`. This entity will be the one that business can change values to modify rules. 
	* **DataFixtures**
		- Each bundle should have data fixutre so it can be loaded after installation.
	* **Resources**
		* docs
			- Should be written in **MarkDown**.
			- Should have the documentation for this bundle from technical perspective and all docs from business to describe the Rule (bundle).
			- Should have example of calls (request and response) including the url.
			- Should have basic flow chart on how this bundle being called, the steps of handling data and which component is used to handle. Example:
				* Data will be converted on `Controller` to `DTO` to deserialize.
				* `Request` will be logged using `EventListener` to `Request`.
				* Data will be validated using `EventListener` of `Request`.
				* Data will be handled using `Service`.
				* `Response` will be sent.
	* **Serializer**
		- Serialize/deserialize input/output data
	* **Service**
	* **Test**
	* **EventListner**
		- on request
		- on response
		- on exceptions 
	* **Validator**
		- Validation for input data
	* **Logger**
		- Logging will be done to `MongoDB`. We experienced lots of issues with flat file logging with Kiddicare. When it comes to debugging it was a very slow and we needed to search and debug. So we will use `mongoDB` this time. This will help us to visualize and monitor the rules engine.

---

## Database Migrations
> to be added

---

## Deployment
> To be added

---

##Releases
> To be added

---

## How to install
> To be added